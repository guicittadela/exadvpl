#INCLUDE "protheus.ch"
#INCLUDE "fileio.ch"

#DEFINE CRLF Chr(13) + Chr(10)

User Function ORGANIZAR
    Local cLine
    Local aArray := {}

    If Ft_FUse("C:\Users\Guilherme\Desktop\file.txt") != -1

        If File("C:\Users\Guilherme\Desktop\newfile.csv")
		    nHandle := FOPEN("C:\Users\Guilherme\Desktop\newfile.csv", FO_READWRITE)
	    Else
		    nHandle := FCREATE("C:\Users\Guilherme\Desktop\newfile" + StrTran(Time(), ":","") + ".csv")
            FWrite(nHandle, "Nome ; CPF" + CRLF)
	    Endif

        Ft_FGoTop()

        While !Ft_FeOf()
            cLine := Ft_FReadLn()
            aArray := StrTokArr2(cLine, " ")
            FSeek(nHandle, 0, FS_END)
		    FWrite(nHandle, aArray[1] + " ; " + aArray[2] + CRLF)
            Ft_FSkip()
        EndDo

        FClose(nHandle)
    Else
        MsgInfo("Arquivo n�o encontrado", "Erro")
    EndIf
Return
