#INCLUDE "protheus.ch"
#INCLUDE "fileio.ch"

User Function NEWEXPORT
    Local cAlias := GetNextAlias()
    Local aArea := GetArea()
    Local cSQL := ""
    Local cNome := ""
    Local cCpf := ""
    Local aAreaSZ0 := SZ0->(GetArea())

    SZ0->(DbSetOrder(2)) // Z0_FILIAL + Z0_NOME + Z0_CPF

    cSQL += " SELECT Z0_NOME, Z0_CPF, Z0_EXPORT "
    cSQL += " FROM " + RetSQLName("SZ0")
    cSQL += " WHERE Z0_FILIAL = '"+ xFilial("SZ0")+ "' "
    cSQL += " AND D_E_L_E_T_ = ' ' "
    cSQL += " AND Z0_NOME <> ' ' "
    cSQL += " AND Z0_EXPORT = 'F' "

    DbUseArea(.t., 'TOPCONN', TcGenqry(,, cSQL), cAlias, .f., .t.)

    If (cAlias)->(EoF())
        MsgInfo("Nenhum dado encontrado", "Aviso")
    Else

        If File("C:\users\Guilherme\Desktop\BD.csv")
            nHandle := FOpen("C:\users\Guilherme\Desktop\BD.csv", FO_READWRITE)
        Else
            nHandle := FCreate("C:\users\Guilherme\Desktop\BD.csv")
            FWrite(nHandle, "Nome ; CPF" + CRLF)
        EndIf

        While !(cAlias)->(EoF())
            cNome := AllTrim((cAlias)->Z0_NOME)
            cCpf := (cAlias)->Z0_CPF

            FSeek(nHandle, 0, FS_END)
            FWrite(nHandle, cNome + " ; " + cCpf + CRLF)

            If SZ0->(MsSeek(xFilial("SZ0") + (cAlias)->Z0_Nome + cCPF))
                SZ0->(RecLock("SZ0", .f.))
                SZ0->Z0_EXPORT := .t.
                SZ0->(MsUnlock())
            Endif

            (cAlias)->(DbSkip())
        EndDo

        FClose(nHandle)
    EndIf

    (cAlias)->(DbCloseArea())

    RestArea(aAreaSZ0)
    RestArea(aArea)
Return
