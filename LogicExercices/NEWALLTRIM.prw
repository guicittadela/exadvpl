#INCLUDE 'protheus.ch'

User Function NEWALLTRIM(cString)
    //Local cString := '          Gui   SDSDS DFSFDS    Cittadela        '
    Local cSeparator := ' '
    Local cValue := ''
    Local cStrValue
    Local nValue
    Local nCount := 0

    For nValue := 1 To Len(cString)
        cStrValue := SubStr(cString, nValue, 1)
        If cStrValue != cSeparator
            cValue += SubStr(cString, nValue, Len(cString))
            Exit
        EndIf
    Next

    For nValue := Len(cValue) To 1 Step -1
        cStrValue := SubStr(cValue, nValue, 1)
        If cStrValue == cSeparator
            nCount++
        Else
            cValue := SubStr(cValue, 1, (Len(cValue)-nCount))
            Exit
        EndIf
    Next
    MsgAlert(cValue)
Return cValue


