#INCLUDE 'protheus.ch'

User Function DIVSUMPAIR()
    Local aArray := { 5, 9, 10, 7, 4 }
    Local nInteger := 2
    Local nValue
    Local nValue2
    Local nArrayValue
    Local nPairs := 0

    For nValue := 1 To Len(aArray)
        nArrayValue := aArray[nValue]
        For nValue2 := (nValue + 1) To Len(aArray)
            If ((nArrayValue + aArray[nValue2]) % nInteger) == 0
                nPairs ++
            EndIf
        Next
    Next
    MsgAlert(Str(nPairs))
Return 
