#INCLUDE 'protheus.ch'

#DEFINE CRLF Chr(13) + Chr(10)


User Function MIGRATORY()
    Local aArray := {1, 2, 3, 4, 5, 4, 3, 2, 1, 3, 4}
    Local nLenArr := Len(aArray)
    Local nValue
    Local nType1 := 0 
    Local nType2 := 0 
    Local nType3 := 0 
    Local nType4 := 0 
    Local nType5 := 0 

    For nValue := 1 To nLenArr
        Do Case
            Case aArray[nValue] == 1
                nType1++

            Case aArray[nValue] == 2
                nType2++

            Case aArray[nValue] == 3
                nType3++

            Case aArray[nValue] == 4
                nType4++

            Case aArray[nValue] == 5
                nType5++
        EndCase
    Next
        
    Do Case
        Case nType1 >= nType2 .and. nType1 >= nType3 .and. nType1 >= nType4 .and. nType1 >= nType5
            MsgInfo("O n�mero maior de passaros � o de tipo 1: " + Alltrim(Str(nType1)) + " p�ssaros")

        Case nType2 > nType1 .and. nType2 >= nType3 .and. nType2 >= nType4 .and. nType2 >= nType5
            MsgInfo("O n�mero maior de passaros � o de tipo 2: " + Alltrim(Str(nType2)) + " p�ssaros")

        Case nType3 > nType1 .and. nType3 > nType2 .and. nType3 >= nType4 .and. nType3 >= nType5
            MsgInfo("O n�mero maior de passaros � o de tipo 3: " + Alltrim(Str(nType3)) + " p�ssaros")

        Case nType4 > nType1 .and. nType4 > nType2 .and. nType4 > nType3 .and. nType4 >= nType5
            MsgInfo("O n�mero maior de passaros � o de tipo 4: " + Alltrim(Str(nType4)) + " p�ssaros")

        Case nType5 > nType1 .and. nType5 > nType2 .and. nType5 > nType3 .and. nType5 > nType4
            MsgInfo("O n�mero maior de passaros � o de tipo 5: " + Alltrim(Str(nType5)) + " p�ssaros")

    EndCase
Return
