#INCLUDE 'protheus.ch'

User Function DAYOFYEAR()
    paramRel()
Return

Static Function paramRel()
    Local aParam := {}
    Local bOk := {|| CalculateDay(MV_PAR01, MV_PAR02)}
    
    AAdd(aParam, {1, "Ano", Space(4),,,,,60,.f. })
    AAdd(aParam, {1, "Dias passados ", Space(3),,,,,60,.f. })

Return ParamBox(aParam, "Parametros",,bOk/*retorna uma caixa de di�logo*/,, .t.,,,, 'DAYOFYEAR', .t., .t.)

Static Function CalculateDay(cYear, cDaysOfDev)
    Local lLeapYear := .f.
    Local aArray:={31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}
    Local nSumDays:= 0
    Local nMonth := 1
    Local nDay := 0
    Local nYear := Val(cYear)
    Local nDaysOfDev := Val(cDaysOfDev)

    Do Case 
        Case nYear < 1918
            If (nYear % 4 == 0)
                lLeapYear := .t.
            EndIf

        Case nYear > 1918
            If (nYear % 4 == 0) .and. (nYear % 100 != 0)
                lLeapYear := .t. 

            EndIf

        Case nYear == 1918
            aArray[2] := 14

    EndCase

    If lLeapYear == .t.
        aArray[2] := 29
    EndIf
    
    If nDaysOfDev <= aArray[1]
        MsgInfo(AllTrim(Str(nDaysOfDev)) + "/" + AllTrim(Str(nMonth)) + "/" + AllTrim(Str(nYear)))
    Else
        While (nSumDays + aArray[nMonth]) <= nDaysOfDev
            nSumdays := nSumdays + aArray[nMonth]
            nMonth++
        EndDo

        If nDaysOfDev == nSumDays
            nMonth --
            nDay := aArray[nMonth]
        Else
            nDay := nDaysOfDev - nSumDays 
        EndIf
        MsgInfo(AllTrim(Str(nDay)) + "/" + AllTrim(Str(nMonth)) + "/" + AllTrim(Str(nYear)))
    EndIf
Return
