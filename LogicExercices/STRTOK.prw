#INCLUDE 'protheus.ch'

#DEFINE CRLF Chr(13) + Chr(10)

User Function STRTOK()
    paramrel()
Return

Static Function Separator(cString, cSeparator)
    Local cValue := ''
    Local cStrValue
    Local nValue
    Local aArray := {}
    Local cResult := ''
    Local cNewString 

    If cSeparator == " "
        cNewString := U_NEWALLTRIM(cString)
    Else
        cNewString := cString
    EndIf

    For nValue := 1 To Len(cNewString)
        cStrValue := SubStr(cNewString, nValue,1)

        If cStrValue != cSeparator
            cValue += cStrValue
        EndIf

        cStrValue := SubStr(cNewString, (nvalue+1), 1)

        If cStrValue == cSeparator .or. cStrValue == ''
            aAdd(aArray, cValue)
            cValue := ''
        EndIf

    Next

    For nValue := 1 To Len(aArray)
        cResult += aArray[nValue] + CRLF
    Next
        MsgInfo(cResult, "Itens do array")
Return

Static Function paramRel()
    Local aParam := {}
    Local bOk := {|| Separator(MV_PAR01, MV_PAR02)}
    
    AAdd(aParam, {1, "Texto", Space(300),,,,,60,.f. })
    AAdd(aParam, {1, "Separador ", Space(1),,,,,60,.f. })

Return ParamBox(aParam, "Parametros",,bOk/*retorna uma caixa de di�logo*/,, .t.,,,, 'STRTOK', .t., .t.)
