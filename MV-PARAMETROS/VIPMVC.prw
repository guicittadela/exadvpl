#INCLUDE 'protheus.ch'
#INCLUDE 'fwmvcdef.ch'

User Function VIPMVC()
    Local oBrowse

    oBrowse := FWMBrowse():New()
    oBrowse:SetAlias('SZ0')
    oBrowse:SetDescription('Fun��es')
    oBrowse:SetMenuDef('VIPMVC')
    oBrowse:DisableDetails()
    oBrowse:Activate()
Return

Static Function MenuDef()
Return FwMVCMenu('VIPMVC') 

Static Function ModelDef()
    Local oModel := MPFormModel():New('VIPS')
    Local oStruSZ0 := FWFormStruct(1, 'SZ0')
    Local oStruSZ2 := FWFormStruct(1, 'SZ2') 
    Local oStruBID := FWFormStruct(1, 'BID') 

    oModel:AddFields('SZ0MASTER',,oStruSZ0)
    oModel:AddGrid('SZ2DETAIL', 'SZ0MASTER', oStruSZ2)
    oModel:AddGrid('BIDDETAIL','SZ0MASTER',oStruBID)
    
    oModel:SetRelation('SZ2DETAIL',{{'Z2_FILIAL', 'xFilial("SZ2")'}, {'Z2_CPF', 'Z0_CPF'}}, SZ2->(IndexKey(1)))
    oModel:SetRelation('BIDDETAIL',{{'BID_FILIAL', 'xFilial("BID")'}, {'BID_CODMUN', 'Z0_CODMUN'}}, BID->(IndexKey(1)))
    oModel:SetDescription('Cadastro de Fun��es')

    oModel:GetModel('SZ0MASTER'):SetDescription('Dados das pessoas')
    oModel:GetModel('SZ2DETAIL'):SetDescription('Detalhes11')
    oModel:GetModel('BIDDETAIL'):SetDescription('Detalhes')
    
    oModel:SetPrimaryKey({'Z0_FILIAL', 'Z0_CPF'})
    oModel:SetPrimaryKey({'Z2_FILIAL', 'Z2_CPF'})
    oModel:SetPrimaryKey({'BID_FILIAL', 'BID_CODMUN'})
Return oModel

Static Function ViewDef()
    Local oModel := FWLoadModel('VIPMVC')
    Local oStruSZ0 := FWFormStruct(2, 'SZ0')
    Local oStruSZ2 := FWFormStruct(2, 'SZ2')
    Local oStruBID := FWFormStruct(2, 'BID')
    Local oView 

    oView := FWFormView():New()
    oView:SetModel(oModel)

    oView:AddField('VIEWSZ0', oStruSZ0, 'SZ0MASTER')
    oView:AddGrid('VIEWSZ2', oStruSZ2, 'SZ2DETAIL')
    oView:AddGrid('VIEWBID', oStruBID, 'BIDDETAIL')
    
    oView:CreateHorizontalBox("SUPERIOR", 60)
    oView:CreateHorizontalBox("INFERIOR", 40)

    oView:CreateFolder('PASTAS', "INFERIOR")

    oView:AddSheet('PASTAS', 'ABA01', 'Hobbies')
    oView:AddSheet('PASTAS', 'ABA02', 'Cidades')

    oView:CreateVerticalBox('ESQUERDA', 100,,, 'PASTAS', 'ABA01')
    oView:CreateVerticalBox('DIREITA', 100,,, 'PASTAS', 'ABA02')

    oView:SetOwnerView('VIEWSZ0', 'SUPERIOR')
    oView:SetOwnerView('VIEWSZ2', 'ESQUERDA')
    oView:SetOwnerView('VIEWBID', 'DIREITA')

Return oView
