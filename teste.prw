#INCLUDE "protheus.ch"

User Function TESTEGUI()
    cDate := "19920130"
    MsgAlert((checkDate(cDate) + 1))
return 

Static Function checkDate(cDate)
    cDay := SubStr(cDate, 7 )
    nDay = VAL(cDay)
    
Return nDay
