#INCLUDE "protheus.ch"
#INCLUDE "fwmvcdef.ch"

User Function MVCGUI2()
    Local oBrowse

    oBrowse := FWMBrowse():New()
    oBrowse:SetAlias('SZ0')
    oBrowse:SetFilterDefault( "Z0_NOME<>' '" )
    oBrowse:SetDescription('Cadastro de Pessoas f�sicas')
    oBrowse:SetMenuDef('MVCGUI2')
    oBrowse:Activate()
Return


Static Function MenuDef()
Return FwMvcMenu("MVCGUI2")


Static Function ModelDef()
    Local oStruSZ0 := FWFormStruct( 1, 'SZ0' )

    Local oModel // Modelo de dados que ser� constru�do

    oModel := MPFormModel():New('Pessoas F�sicas',,{|oModel|MyValid(oModel)} ,{|oModel|MyCommit(oModel)} )
    oModel:AddFields( 'SZ0MASTER', /*cOwner*/, oStruSZ0)
    oModel:SetDescription( 'Lista de Pessoas f�sicas' )
    oModel:SetPrimaryKey( {"ZO_FILIAL", "Z0_CPF"} )

Return oModel


Static Function ViewDef()
    Local oModel := FWLoadModel( 'MVCGUI2' )
    Local oStruSZ0 := FWFormStruct( 2, 'SZ0' )
    Local oView

    oView := FWFormView():New()
    oView:SetModel(oModel)
    oView:AddField('VIEW_SZ0', oStruSZ0, 'SZ0MASTER')
    oView:CreateHorizontalBox( 'TELA' , 100 )
    oView:SetOwnerView( 'VIEW_SZ0', 'TELA' )
Return oView

Static Function MyCommit(oModel as Object)
    If oModel:GetOperation() == MODEL_OPERATION_UPDATE
        oModel:SetValue('SZ0MASTER', 'Z0_EXPORT', .f.)
    EndIf
Return FWFormCommit(oModel)

Static Function MyValid(oModel as Object)
    Local cCpf As Character
    Local lValid := .f.

    cCpf := oModel:GetValue('SZ0MASTER', 'Z0_CPF')

    Do Case
        Case oModel:GetOperation() == MODEL_OPERATION_UPDATE
            If inputCheck(oModel)
                lValid := .t.
            Else
                If checKCpf(cCpf)
                    lValid := .f.
                    Help("",1,"CPF j� cadastrado",,"Verifique os regitros na tabela",1)
                Else
                    lValid := .t.
                EndIf
            EndIf

        Case oModel:GetOperation() == MODEL_OPERATION_INSERT

            If checKCpf(cCpf)
                lValid := .f.
                Help("",1,"CPF j� cadastrado",,"Verifique os regitros na tabela",1)
            Else
                lValid := .t.
            EndIf
    EndCase
Return lValid


Static Function checKCpf(cCpf as Character)
    Local lFind
    lFind := .f.

    SZ0->(DbSetOrder(1))
    If SZ0->(MsSeek(xFilial("SZ0") + cCPF))
        lFind := .t.
    Else
        lFind := .f.
    EndIf
Return lFind


Static Function inputCheck(oModel as Object)
    Local cName as Character
    Local cCode as Character
    Local lRet := .f.

    cName := oModel:GetValue('SZ0MASTER', 'Z0_NOME')
    cCode := oModel:GetValue('SZ0MASTER', 'Z0_CODMUN')

    Do Case
        Case AllTrim(cName) != AllTrim(SZ0->Z0_NOME)
            lRet := .t.

        Case AllTrim(cCode) != AllTrim(SZ0->Z0_CODMUN)
            lRet := .t.
    EndCase
Return lRet
