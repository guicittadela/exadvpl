#INCLUDE 'protheus.ch'
#INCLUDE 'fwmvcdef.ch'

User function FUNCMVC()
    Local oBrowse

    oBrowse := FWMBrowse():New()
    oBrowse:SetAlias('SZ0')
    oBrowse:SetDescription('Cadastro de Fun��es')
    oBrowse:SetMenuDef('FUNCMVC')
    oBrowse:Activate()
Return

Static Function MenuDef()
Return FwMVCMenu('FUNCMVC') 

Static Function ModelDef()
    Local oModel := MPFormModel():New('Fun��es') 
    Local oStruSZ0 := FWFormStruct(1, 'SZ0')
    Local oStruSZ2 := FWFormStruct(1, 'SZ2')

    oModel:AddFields('SZ0MASTER', /*OWNER*/, oStruSZ0)
    oModel:AddGrid('SZ2DETAIL', 'SZ0MASTER', oStruSZ2)

    oModel:SetRelation('SZ2DETAIL', {{'Z2_FILIAL', 'xFilial("SZ2")'}, {'Z2_CPF', 'Z0_CPF'}}, SZ2->(IndexKey(1)))
    
    oModel:SetDescription('Cadastro de Fun��es')

    oModel:GetModel('SZ0MASTER'):SetDescription('Dados das pessoas')
    oModel:GetModel('SZ2DETAIL'):SetDescription('Detalhes')
    
    oModel:SetPrimaryKey({'Z0_FILIAL', 'Z0_CPF'})
    oModel:SetPrimaryKey({'Z2_FILIAL', 'Z2_CPF'})
Return oModel

Static Function ViewDef()
    Local oModel := FWLoadModel('FUNCMVC')
    Local oStruSZ0 := FWFormStruct(2, 'SZ0')
    Local oStruSZ2 := FWFormStruct(2, 'SZ2')
    Local oView

    oView := FWFormView():New()
    oView:SetModel(oModel)
    
    oView:AddField('VIEWSZ0', oStruSZ0, 'SZ0MASTER')
    oView:AddGrid('VIEWSZ2',oStruSZ2, 'SZ2DETAIL')
    
    oView:CreateHorizontalBox('SUPERIOR', 30)
    oView:CreateHorizontalBox('INFERIOR', 70)
    
    oView:SetOwnerView('VIEWSZ0', 'SUPERIOR')
    oView:SetOwnerView('VIEWSZ2', 'INFERIOR')
Return oView
