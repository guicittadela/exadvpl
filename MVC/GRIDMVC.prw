#INCLUDE "protheus.ch"
#INCLUDE "fwmvcdef.ch"

User Function GRIDMVC()
    Local oBrowse

    oBrowse := FWMBrowse():New()
    oBrowse:SetAlias('SZ0')
    oBrowse:SetDescription('Cadastro de Pessoas f�sicas')
    oBrowse:SetMenuDef('GRIDMVC')
    oBrowse:Activate()
Return

Static Function MenuDef()
Return FwMvcMenu('GRIDMVC')


Static Function ModelDef()
    Local oModel := MPFormModel():New('Pessoas')  //cria o modelo
    Local oStruSZ0 := FWFormStruct(1,'SZ0')
    Local oStruBID := FWFormStruct(1,'BID')

    oModel:AddFields('SZ0MASTER',/*owner*/,oStruSZ0) //adiciona os campos da tabela sz0
    oModel:AddGrid('BIDDETAIL','SZ0MASTER', oStruBID) //adiciona o grid para a tabela BID

    oModel:SetRelation('BIDDETAIL',{ {'BID_FILIAL','xFilial("BID")'}, {'BID_CODMUN', 'Z0_CODMUN'} }, BID->(IndexKey(1))) //seta as rela��es entre as tabelas sz0 e bid

    oModel:SetDescription( 'Cadastro de pessoas' )

    oModel:GetModel( 'SZ0MASTER' ):SetDescription( 'Dados das pessoas' )
    oModel:GetModel( 'BIDDETAIL' ):SetDescription( 'Dados das cidades' )

    oModel:SetPrimaryKey({'Z0_FILIAL', 'Z0_CODMUN'})
    oModel:SetPrimaryKey({'BID_FILIAL', 'BID_CODMUN'})

Return oModel


Static Function ViewDef()
    Local oModel := FWLoadModel('GRIDMVC')
    Local oStruSZ0 := FWFormStruct(2, 'SZ0')
    Local oStruBID := FWFormStruct(2, 'BID')
    Local oView

    oView := FWFormView():New()
    oView:SetModel(oModel)

    oView:AddField('VIEWSZ0', oStruSZ0, 'SZ0MASTER')
    oView:AddGrid('VIEWBID', oStruBID, 'BIDDETAIL')

    oView:CreateHorizontalBox('SUPERIOR', 50)
    oView:CreateHorizontalBox('INFERIOR', 50)

    oView:SetOwnerView('VIEWSZ0', 'SUPERIOR')
    oView:SetOwnerView('VIEWBID', 'INFERIOR')
Return oView
