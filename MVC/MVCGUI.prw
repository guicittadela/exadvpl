#INCLUDE "protheus.ch"
#INCLUDE "fwmvcdef.ch"

User Function MVCGUI()
    Local oBrowse

    oBrowse := FWMBrowse():New()
    oBrowse:SetAlias('BID')
    oBrowse:SetDescription('Lista de cidades')
    oBrowse:SetMenuDef('MVCGUI')
    oBrowse:DisableDetails()

    oBrowse:Activate()
Return


Static Function MenuDef()
    Local aRotina := {}

    ADD OPTION aRotina Title 'Incluir'    Action 'VIEWDEF.MVCGUI' OPERATION MODEL_OPERATION_INSERT ACCESS 0
    ADD OPTION aRotina Title 'Visualizar' Action 'VIEWDEF.MVCGUI' OPERATION MODEL_OPERATION_VIEW ACCESS 0
    ADD OPTION aRotina Title 'Imprimir' Action 'U_TRELCITY()' OPERATION 8 ACCESS 0
    ADD OPTION aRotina Title 'Copiar' Action 'VIEWDEF.MVCGUI' OPERATION 9 ACCESS 0
    ADD OPTION aRotina Title 'Exportar' Action 'U_CSVEXPORT' OPERATION 2 ACCESS 0
Return aRotina



Static Function ModelDef()
    Local oStruBID := FWFormStruct( 1, 'BID' )
    Local oModel // Modelo de dados que ser� constru�do

    oModel := MPFormModel():New('Cidades' )
    oModel:AddFields( 'BIDMASTER', /*cOwner*/, oStruBID)
    oModel:SetDescription( 'Lista de cidades' )
    oModel:GetModel( 'BIDMASTER' ):SetDescription( 'Cidades' )
    oModel:SetPrimaryKey( {"BID_FILIAL", "BID_CODMUN"} )
Return oModel


/*/{Protheus.doc} ViewDef
Metodo padrao da View MVC

@type function
@author Guilherme
@since 31/01/2022
@version 1.0

@return object, Instancia da classe FwFormView
/*/
Static Function ViewDef()
    Local oModel := FWLoadModel( 'MVCGUI' )
    Local oStruBID := FWFormStruct( 2, 'BID' )
    Local oView

    oView := FWFormView():New()
    oView:SetModel( oModel )
    oView:AddField( 'VIEW_BID', oStruBID, 'BIDMASTER' )
    oView:CreateHorizontalBox( 'TELA' , 100 )
    oView:SetOwnerView( 'VIEW_BID', 'TELA' )
Return oView


