#INCLUDE "protheus.ch"
#INCLUDE "fileio.ch"

User Function CSVTOBD
    Local aAreaSZ0 := SZ0->(GetArea())
    Local cLine := ""
    Local aArray := {}

    If Ft_FUse("c:\users\Guilherme\desktop\bd.csv") != -1

        Ft_FSkip()

        While !Ft_FeOf()
            cLine := Ft_FReadLn()
            aArray := StrTokArr2(cLine, ",")

            SZ0->(ReClock("SZ0", .t.))
            SZ0->Z0_FILIAL := xFilial("SZ0")
            SZ0->Z0_NOME := aArray[1]
            SZ0->Z0_CPF := aArray[2]
            SZ0->(MsUnlock())

            Ft_FSkip()
        EndDo

        Ft_FUse()

    Else
        MsgInfo("Arquivo n�o encontrado", "Erro")
    EndIf

    RestArea(aAreaSZ0)
Return
