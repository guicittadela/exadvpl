#INCLUDE "protheus.ch"
#INCLUDE "fileio.ch"

#DEFINE CRLF Chr(13) + Chr(10)

User Function files
    Local nHandle

    nHandle := FOpen( "C:\Users\Guilherme\Desktop\file.txt", FO_READWRITE)

    If nHandle==-1
        MsgStop("Erro de abertura", "Erro")
    Else
        FSeek( nHandle, 0, FS_END )
        FWrite(nHandle, "nova linha" + CRLF)
        FClose()
    EndIf
Return
