#INCLUDE "protheus.ch"
#INCLUDE "fileio.ch"

#DEFINE CRLF Chr(13) + Chr(10)

User Function CONSULTADB
    Local cAlias := GetNextAlias()
    Local cSQL := ""
    Local cNome := ""
    Local cCPF := ""

    cSQL += " SELECT Z0_NOME, Z0_CPF "
    cSQL += " FROM " + RetSQLName("SZ0")
    cSQL += " WHERE D_E_L_E_T_ = ' ' "

    DbUseArea(.t., 'TOPCONN', TcGenqry(,, cSQL), cAlias, .f., .t.)

    If (cAlias)->(EoF())
        MsgInfo("Nenhum dado encontrado", "Aviso")
    Else
        If File("C:\users\Guilherme\Desktop\BD.csv")
            nHandle := FOpen("C:\users\Guilherme\Desktop\BD.csv", FO_READWRITE)
        Else
            nHandle := FCreate("C:\users\Guilherme\Desktop\BD.csv")
            FWrite(nHandle, "Nome ; CPF" + CRLF)
        EndIf

        While !(cAlias)->(EoF())
            cNome := AllTrim((cAlias)->Z0_NOME)
            cCPF := (cAlias)->Z0_CPF
            FSeek(nHandle, 0, FS_END)
            FWrite(nHandle, cNome + " ; " + cCPF + CRLF)
            (cAlias)->(DbSkip())
        EndDo
    EndIf
    (cAlias)->(DbCloseArea())
Return
