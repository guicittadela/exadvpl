#INCLUDE "protheus.ch"
#INCLUDE "fileio.ch"

#DEFINE CRLF Chr(13) + Chr(10)

User Function CSVTOTXT
    Local cLine := ""
    Local aArray := {}

    If Ft_FUse("C:\Users\Guilherme\Desktop\newfile.csv") != -1

        Ft_FGoTop()
        Ft_FSkip()

        If File("C:\Users\Guilherme\Desktop\file.txt")
            nHandle := FOpen("C:\Users\Guilherme\Desktop\file.txt", FO_READWRITE)
        Else
            nHandle := FCreate("C:\Users\Guilherme\Desktop\file.txt")
        EndIf

        While !Ft_FeOf()
            cLine := Ft_FReadLn()
            aArray := StrTokArr2(cLine, ";")

            FSeek(nHandle, 0, FS_END)
            FWrite(nHandle, aArray[1] + " " + aArray[2] + CRLF)

            Ft_FSkip()
        EndDo

        FClose()
    Else
        MsgInfo("Arquivo n�o encontrado", "Erro")
    EndIf

Return


