#INCLUDE "protheus.ch"
#INCLUDE "fileio.ch"

#DEFINE CRLF Chr(13) + Chr(10)

User Function EXPORTACAO()
	Local nHandle2

	nHandle := FT_FUse( "C:\Users\Guilherme\Desktop\file.txt" )
	If nHandle == -1
		MsgInfo("Erro", "Erro")
	Endif

	If File("C:\Users\Guilherme\Desktop\newfile.txt")
		nHandle2 := FOpen("C:\Users\Guilherme\Desktop\newfile.txt", FO_READWRITE)
	Else
		nHandle2 := FCerate("C:\Users\Guilherme\Desktop\newfile.txt")
	Endif

	FT_FGoTop()

	While !FT_FEof()
		cLine := FT_FReadLn()
		FSeek(nHandle2, 0, FS_END)
		FWrite(nHandle2, cLine + CRLF)

		FT_FSkip()
	EndDo

    FClose(nHandle2)

    Ft_FUse()
Return
